import { Application } from "./application";
import { Game, makeTestGame } from "./game";
import { SceneRenderer } from "./SceneRenderer";
import { Vector3 } from "three";
import { InterfaceRenderer } from "./InterfaceRenderer";

const moveVectors = [
    new Vector3(1, 0, 0),
    new Vector3(0, 0, -1),
    new Vector3(-1, 0, 0),
    new Vector3(0, 0, 1),
]

const moveBindings: {[key: string]: number} = {
    "ArrowRight": 0,
    "ArrowLeft": 2,
    "ArrowDown": 3,
    "ArrowUp": 1,
};

export default class Player
{
    public readonly testGame: Game = makeTestGame();
    
    private inputCooldown = 0;

    constructor(private readonly app: Application,
                private readonly sceneRenderer: SceneRenderer,
                private readonly interfaceRenderer: InterfaceRenderer)
    {
        this.sceneRenderer.rebuildSceneFromGame(this.testGame);
        this.app.updates.push(dt => this.update(dt));
        this.sceneRenderer.pivot.distance = 8;
    }

    public update(dt: number): void
    {
        this.checkInput(dt);

        //this.sceneRenderer.pivot.angle += Math.PI * dt * .01;
        this.sceneRenderer.pivot.focus.set(this.testGame.avatar.position[0],
                                           this.testGame.avatar.position[1],
                                           this.testGame.avatar.position[2]);
    }

    private checkInput(dt: number): void
    {
        this.inputCooldown -= Math.min(dt, this.inputCooldown);
            
        if (this.inputCooldown > 0) return;

        for (const key in moveBindings)
        {
            if (this.app.heldKeys.get(key))
            {
                if (this.interfaceRenderer.isDialogueActive)
                {
                    this.interfaceRenderer.advanceDialogue();
                }
                else
                {
                    const angle = this.sceneRenderer.pivot.angle;
                    const offset = Math.round(2 * angle / Math.PI);
                    const index = (moveBindings[key] + offset) % 4;
                    this.tryMoveAvatar(moveVectors[index]);
                }

                this.inputCooldown = .4;
            }
        }
    }

    private tryMoveAvatar(vector: Vector3): void
    {
        const dest: [number, number, number] = [
            this.testGame.avatar.position[0] + vector.x,
            this.testGame.avatar.position[1] + vector.y,
            this.testGame.avatar.position[2] + vector.z
        ];
    
        const destVec = new Vector3(...dest);
        const destCell = this.testGame.testHeights.get(destVec.setY(0));
        
        if (destCell) {
            dest[1] = destCell[0].y;
        }

        const character = this.testGame.getCharacterAt(dest);

        if (!character)
        {
            this.testGame.avatar.position = dest;
            this.sceneRenderer.rebuildSceneFromGame(this.testGame);
        }
        else
        {
            this.interfaceRenderer.queueDialogueScript(character.dialogue);
        }
    }
}
