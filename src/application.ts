import { WebGLRenderer, Clock } from "three";

export class Application
{
    public readonly threeRenderer = new WebGLRenderer({ antialias: false });
    
    public readonly updates: ((dt: number) => void)[] = [];
    public readonly renders: (() => void)[] = [];

    public readonly heldKeys = new Map<string, boolean>();

    public readonly resolution: [number, number] = [256, 256];

    private readonly clock = new Clock(true);

    constructor(public readonly document: HTMLDocument,
                public readonly canvasRoot: HTMLElement)
    {
        this.setupKeyListeners();
        this.setupRenderers();
    }

    private setupRenderers()
    {
        this.threeRenderer.setPixelRatio(window.devicePixelRatio);
        this.threeRenderer.autoClear = false;
        this.canvasRoot.appendChild(this.threeRenderer.domElement);

        window.addEventListener('resize', () => this.onResized(), false);
        this.threeRenderer.setAnimationLoop(() => this.update());

        this.onResized();
    }

    private update(): void
    {
        const dt = this.clock.getDelta();
        this.updates.forEach(update => update(dt));
        this.render();
    }

    private render(): void
    {
        this.threeRenderer.clear();
        this.renders.forEach(render => render());
    }

    private setupKeyListeners()
    {
        this.document.addEventListener("keydown", event =>
        {
            this.heldKeys.set(event.key, true);
        });

        this.document.addEventListener("keyup", event =>
        {
            this.heldKeys.set(event.key, false);
        });
    }

    private onResized(): void
    {
        const [width, height] = this.resolution;
        this.threeRenderer.setSize(width, height, false);
    }
}
