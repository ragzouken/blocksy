export type Room = { blocks: RoomBlock[]; }
export type RoomBlock = { position: RoomCoord, orientation: BlockOrientation,  };
export type RoomCoord = [number, number, number];
export type BlockOrientation = number;

export const testRoom = generateTestRoom();

function makeBlock(x: number, y: number, z: number): RoomBlock {
    return {
        position: [x, y, z],
        orientation: 0,
    };
}

function generateTestRoom(): Room {
    const room: Room = { blocks: [] };
    
    for (let y = -7; y < 8; ++y) {
        for (let x = -7; x < 8; ++x) {
            room.blocks.push(makeBlock(x, -1, y));
        }
    }

    for (let i = -7; i < 8; ++i) {
        room.blocks.push(makeBlock(i, 0, -7));
        room.blocks.push(makeBlock(i, 0,  8));
        room.blocks.push(makeBlock(-7, 0, i));
        room.blocks.push(makeBlock( 8, 0, i));
    }

    return room;
}
