import { PerspectiveCamera, Vector3, Euler } from "three";

export class PivotCamera
{
    public focus = new Vector3(0, 0);
    public angle = 0;
    public pitch = -Math.PI / 8;
    public distance = 12;
    
    private readonly rotation = new Euler();

    public updateCamera(camera: PerspectiveCamera): void
    {
        this.rotation.set(this.pitch, this.angle, 0, "ZYX");

        camera.position.set(0, 0, this.distance);
        camera.position.applyEuler(this.rotation);
        camera.position.addVectors(camera.position, this.focus);
        
        camera.lookAt(this.focus);
    }
}

export function delay(time: number)
{
    return new Promise(resolve => setTimeout(() => resolve(), time));
}

export function clamp(min: number, max: number, value: number): number
{
    return Math.max(min, Math.min(max, value));
}

export function lerp(v0: number, v1: number, t: number): number
{
    return (1 - t) * v0 + t * v1;
}

export function invLerp(v0: number, v1: number, v: number): number
{
    return (v - v0) / (v1 - v0);
}

export function randomInt(min: number, max: number)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function num2hex(value: number): string
{
    return rgb2hex(num2rgb(value));
}

export function rgb2num(r: number, g: number, b: number, a: number = 255)
{
  return ((a << 24) | (b << 16) | (g << 8) | (r)) >>> 0;
}

export function num2rgb(value: number): [number, number, number]
{
    const r = (value >>  0) & 0xFF;
    const g = (value >>  8) & 0xFF;
    const b = (value >> 16) & 0xFF;
    
    return [r, g, b];
}

export function rgb2hex(color: [number, number, number]): string
{
    const [r, g, b] = color;
    let rs = r.toString(16);
    let gs = g.toString(16);
    let bs = b.toString(16);

    if (rs.length < 2) { rs = "0" + rs; }
    if (gs.length < 2) { gs = "0" + gs; }
    if (bs.length < 2) { bs = "0" + bs; }

    return `#${rs}${gs}${bs}`;
}

export function hex2rgb(color: string): [number, number, number]
{
    const matches = color.match(/^#([0-9a-f]{6})$/i);

    if (matches) 
    {
        const match = matches[1];

        return [
            parseInt(match.substr(0,2),16),
            parseInt(match.substr(2,2),16),
            parseInt(match.substr(4,2),16)
        ];
    }
    
    return [0, 0, 0];
}
