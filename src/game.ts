import { randomInt } from "./utility";
import { Vector3 } from "three";
import { CellMap } from "./utility/grid";

export class Character
{
    constructor (public position: [number, number, number] = [0, 0, 0],
                 public dialogue: string = "")
    {   
    }
}

export function makeTestGame(): Game
{
    const game = new Game();
    game.characters.length = 0;

    const moveVectors = [
        new Vector3(1, 0, 0),
        new Vector3(0, 0, -1),
        new Vector3(-1, 0, 0),
        new Vector3(0, 0, 1),
    ];

    const tst = game.testHeights;
    for (let y = -8; y < 8; ++y) {
        for (let x = -8; x < 8; ++x) {
            const vector = new Vector3(x, randomInt(0, 2) * randomInt(0, 2), y);
            const coord = vector.clone().setY(0);
            tst.set(coord, [vector, vector, vector, vector, vector]);
        }
    }

    tst.forEach(neighbours => {
        const origin = neighbours[0];
        moveVectors.forEach((offset, i) => {
            const neighbour = origin.clone().add(offset).setY(0);
            const cell = tst.get(neighbour);
            if (cell && Math.abs(origin.y - cell[0].y) <= 1) {
                neighbours[i + 1] = cell[0];
            }
        });
    });

    for (let i = 0; i < 8; ++i) {
        const x = randomInt(-7, 7);
        const y = randomInt(-7, 7);
        const cel = tst.get(new Vector3(x, 0, y))!;
        const character = new Character([x, cel[0].y, y], "this is {+r}{+red}blood{-r} world{-red}{el}{delay=.5}it's real{-delay}" );// "{+wvy}hello{-wvy}{ep}i am\na {delay=.5}very{-delay} {+shk}strange{-shk} {+r}{+red}bear{-red}{-r} adsfas df asdf asd fas s");
        game.characters.push(character);
    }

    game.avatar = game.characters[0];


    return game;
}

export class Game
{
    public characters: Character[] = [];
    public avatar: Character;
    public testHeights = new CellMap<Vector3[]>();

    constructor()
    {
        for (let i = 0; i < 8; ++i)
        {
            const x = randomInt(-7, 8);
            const y = randomInt(-7, 8);
            const character = new Character([x, 0, y], "this is {+r}{+red}blood{-r} world{-red}{el}{delay=.5}it's real{-delay}" );// "{+wvy}hello{-wvy}{ep}i am\na {delay=.5}very{-delay} {+shk}strange{-shk} {+r}{+red}bear{-red}{-r} adsfas df asdf asd fas s");
            this.characters.push(character);
        }

        this.avatar = this.characters[0];
    }

    public getCharacterAt(position: [number, number, number]): Character | undefined
    {
        return this.characters.find(character => character.position[0] === position[0]
                                              && character.position[1] === position[1]
                                              && character.position[2] === position[2]);
    }
}
