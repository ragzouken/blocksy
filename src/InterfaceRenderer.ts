import { Application } from "./application";
import { Scene, OrthographicCamera } from "three";

import * as THREE from 'three';
import { DialogueRenderer } from "./DialogueRenderer";
import { Font } from "blitsy";
var ScreenQuad = require('three-screen-quad')(THREE)

export class InterfaceRenderer
{
    private readonly dialogueRenderer: DialogueRenderer;
    private readonly camera = new OrthographicCamera(0, 0, 0, 0);
    private readonly scene = new Scene();

    private dialogueQuad: any;

    public get isDialogueActive() { return !this.dialogueRenderer.empty; }
    
    constructor(private readonly app: Application,
                private readonly font: Font,
                arrow: CanvasImageSource,
                square: CanvasImageSource)
    {
        this.dialogueRenderer = new DialogueRenderer(arrow, square);

        this.setupText();
        this.updateText(0);

        app.updates.push(dt => this.update(dt));
        app.renders.push(() => this.render());
    }

    public queueDialogueScript(text: string): void
    {
        this.dialogueRenderer.queueScript(text);
    }

    public advanceDialogue(): void
    {
        this.dialogueRenderer.skip();
        this.updateText(0);
    }

    public closeDialogue(): void
    {
        this.updateText(0);
    }

    public cancelDialogue(): void
    {
        this.dialogueRenderer.cancel();
    }

    private update(dt: number): void
    {
        this.updateText(dt);
    }

    private render(): void
    {
        const [width, height] = this.app.resolution;
        this.dialogueQuad.setScreenSize(width, height);
        this.app.threeRenderer.clearDepth();
        this.app.threeRenderer.render(this.scene, this.camera);
    }

    private setupText(): void
    {
        this.dialogueRenderer.setFont(this.font);

        const width = this.dialogueRenderer.canvas.width;
        const height = this.dialogueRenderer.canvas.height;

        this.dialogueQuad = new ScreenQuad({
            width: width + 'px',
            height: height + 'px',
            bottom: '24px',
            left: '24px',
            texture: this.dialogueRenderer.texture,
        });

        this.scene.add(this.dialogueQuad);
    }

    private updateText(dt: number): void
    {
        this.dialogueRenderer.update(dt);
        this.dialogueQuad.visible = !this.dialogueRenderer.empty;
        this.dialogueRenderer.render();
    }
}
