import { Vector3 } from "three";

export class CellMap<V>
{
    private readonly hash2key = new Map<string, Vector3>();
    private readonly hash2val = new Map<string, V>();

    public get size(): number { return this.hash2key.size; }

    public get(key: Vector3): V | undefined
    {
        return this.hash2val.get(this.hashKey(key));
    }

    public has(key: Vector3): boolean
    {
        return this.hash2val.has(this.hashKey(key));
    }

    public set(key: Vector3, value: V): this
    {
        const hash = this.hashKey(key);
        this.hash2key.set(hash, key);
        this.hash2val.set(hash, value);
        return this;
    }

    public clear(): void 
    {
        this.hash2key.clear();
        this.hash2val.clear(); 
    }

    public delete(key: Vector3): boolean
    {
        const hash = this.hashKey(key);

        return this.hash2key.delete(hash)
            && this.hash2val.delete(hash);
    }

    public forEach(callbackfn: (value: V, key: Vector3, map: CellMap<V>) => void, thisArg?: any): void
    {
        this.hash2key.forEach((key, hash) => {
            callbackfn(this.hash2val.get(hash)!, key, this);
        });
    }

    private hashKey(key: Vector3): string
    {
        return `${key.x},${key.y},${key.z}`;
    }
}
