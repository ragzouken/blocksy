varying vec2 vUv;
uniform sampler2D uColorIndexMap;
uniform sampler2D uPalette;

void main(void)
{
    float index = texture2D(uColorIndexMap, vUv).r;
    gl_FragColor = texture2D(uPalette, vec2(index, .5));
}
