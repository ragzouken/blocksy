import { Scene, PerspectiveCamera, GridHelper, Color, TextureLoader, Texture, NearestFilter, PlaneGeometry, MeshBasicMaterial, Mesh, LineBasicMaterial, Geometry, LineSegments, BoxBufferGeometry, CanvasTexture, ShaderMaterial } from "three";
import { PivotCamera, randomInt, rgb2hex } from "./utility";
import { Game } from "./game";
import { Application } from "./application";
import { TextureData, decodeTexture, encodeTexture, createContext2D, imageToContext } from "blitsy";
import textureData from "./resources/texture-data";

const loader = new TextureLoader();

function loadTestTexture(url: string): Texture
{
    const texture = loader.load(url);
    texture.minFilter = NearestFilter;
    texture.magFilter = NearestFilter;

    return texture;
}

function dataToTexture(data: TextureData): CanvasTexture
{
    const context = decodeTexture(data);
    const texture = new CanvasTexture(context.canvas as HTMLCanvasElement);
    texture.minFilter = NearestFilter;
    texture.magFilter = NearestFilter;
    return texture;
}

function tilesetToTiles(data: TextureData): CanvasTexture[]
{
    const tileset = decodeTexture(data);
    const tiles: CanvasTexture[] = [];
    for (let i = 0; i < 32; ++i) {
        const context = createContext2D(32, 32);
        context.drawImage(tileset.canvas, i * 32, 0, 32, 32, 0, 0, 32, 32);
        const texture = new CanvasTexture(context.canvas);
        texture.minFilter = NearestFilter;
        texture.magFilter = NearestFilter;
        tiles.push(texture);
    }
    return tiles;
}

export class SceneRenderer
{
    public readonly scene = new Scene();
    public readonly camera = new PerspectiveCamera();
    public readonly pivot = new PivotCamera();

    public pathLines: Geometry = new Geometry();
    public testTexture: Texture;
    public wallTemplates: Mesh[] = [];
    public characters: Mesh[] = [];
    public walls: Mesh[] = [];

    private paletteContext: CanvasRenderingContext2D;
    private paletteTexture: CanvasTexture;
    private paletteMaterial: ShaderMaterial;
    private wallTextures: Texture[] = [];

    private userTexture: CanvasTexture;
    private userContext: CanvasRenderingContext2D;

    constructor(private readonly app: Application)
    {
        this.paletteContext = createContext2D(16, 1);

        this.userContext = createContext2D(16, 16);
        this.userTexture = new CanvasTexture(this.userContext.canvas);
        this.userTexture.minFilter = NearestFilter;
        this.userTexture.magFilter = NearestFilter;

        this.testTexture = dataToTexture(textureData["bear"]);
        const wall = dataToTexture(textureData["wall"]);
        const wall2 = dataToTexture(textureData["water"]);
        
        this.paletteTexture = new CanvasTexture(this.paletteContext.canvas);
        this.paletteTexture.minFilter = NearestFilter;
        this.paletteTexture.magFilter = NearestFilter;

        this.wallTextures.push(this.userTexture);
        this.wallTextures.push(wall);
        this.wallTextures.push(wall2);
        this.wallTextures.push(this.paletteTexture);

        this.paletteMaterial = new ShaderMaterial();

        // lines renderer
        const linesMaterial = new LineBasicMaterial({ color: 0x0000ff });

        // walls
        const wallGeo = new BoxBufferGeometry(1, 1);
        wallGeo.translate(0, .5, 0);

        const wallMat4 = new MeshBasicMaterial({ map: this.userTexture });
        this.wallTemplates.push(new Mesh(wallGeo, wallMat4));

        const wallMat = new MeshBasicMaterial({ map: wall });
        this.wallTemplates.push(new Mesh(wallGeo, wallMat));

        const wallMat2 = new MeshBasicMaterial({ map: wall2 });
        this.wallTemplates.push(new Mesh(wallGeo, wallMat2));

        const wallMat3 = new MeshBasicMaterial({ map: this.paletteTexture });
        this.wallTemplates.push(new Mesh(wallGeo, wallMat3));

        const tiles = tilesetToTiles(textureData["tileset"]);
        tiles.forEach(tile => {
            const mat = new MeshBasicMaterial({ map: tile });
            this.wallTextures.push(tile);
            this.wallTemplates.push(new Mesh(wallGeo, mat));
        });

        const line = new LineSegments(this.pathLines, linesMaterial);
        this.scene.add(line);

        function printImageData(image: HTMLImageElement, format = "R4") {
            const context = imageToContext(image);
            const data = encodeTexture(context, format);
            console.log(JSON.stringify(data));
        }

        //const arrow = loadTestTexture("dialogue-square.png");

        loader.manager.onLoad = () => {
            //printImageData(arrow.image, "M1");
        };

        // grid renderer
        const gridRenderer = new GridHelper(16, 16);
        gridRenderer.position.set(.5, 0, .5);
        //this.scene.add(gridRenderer);
        this.scene.background = new Color(.1, .1, .1);

        this.resize();

        this.app.updates.push(dt => this.update(dt));
        this.app.renders.push(() => this.render());

        //
        const input = document.getElementById("texture-input") as HTMLTextAreaElement;
        const data = this.userContext.createImageData(16, 16);
        data.data.fill(128);
        this.userContext.putImageData(data, 0, 0);
        this.userTexture.needsUpdate = true;

        input.value = encodeTexture(this.userContext, "R4").data;

        input.addEventListener("change", () => {
            const context = decodeTexture({_type:"texture", format:"R4", width: 16, height: 16, data:input.value })
            this.userContext.drawImage(context.canvas, 0, 0);
            this.userTexture.needsUpdate = true;
        });
    }

    public setPaletteMaterial(vertSource: string, fragSource: string): void
    {
        this.paletteMaterial = new ShaderMaterial({
            uniforms: {
                "uColorIndexMap": { value: this.testTexture },
                "uPalette": { value: this.paletteTexture },
            },
            vertexShader: vertSource,
            fragmentShader: fragSource,
            transparent: true,
        });

        this.wallTemplates.forEach((mesh, i) => {
            const material = this.paletteMaterial.clone();
            mesh.material = material;
            material.uniforms = {
                "uColorIndexMap": { value: this.wallTextures[i] },
                "uPalette": { value: this.paletteTexture },
            };
        });
    }

    public randomiseColors(): void
    {
        const i = randomInt(0, 16);
        
        this.paletteContext.fillStyle = rgb2hex([
            randomInt(32, 255),
            randomInt(32, 255),
            randomInt(32, 255),
        ])
        this.paletteContext.fillRect(i, 0, 1, 1);
        
        this.paletteContext.clearRect(0, 0, 1, 1);
        this.paletteTexture.needsUpdate = true;
    }

    private timer: number = 0;
    private update(dt: number): void
    {
        this.resize();

        this.timer += dt;
        if (this.timer >= .1) {
            this.randomiseColors();
            this.timer %= .1;
        }
    }

    private render(): void
    {
        this.pivot.updateCamera(this.camera);
        this.characters.forEach(mesh => this.billboardCharater(mesh));

        this.app.threeRenderer.render(this.scene, this.camera);
    }

    private resize(): void
    {
        const [width, height] = this.app.resolution;

        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    }

    public rebuildSceneFromGame(game: Game): void
    {
        this.characters.forEach(mesh => this.scene.remove(mesh));
        this.characters.length = 0;
        this.walls.forEach(wall => this.scene.remove(wall));
        this.walls.length = 0;

        game.characters.forEach(character =>
        {
            var renderer = this.makeCharacterRenderer();
            renderer.translateX(character.position[0]);
            renderer.translateY(character.position[1]);
            renderer.translateZ(character.position[2]);
            this.scene.add(renderer);
            this.characters.push(renderer);
        });

        game.testHeights.forEach(vectors => {
            const origin = vectors[0];
            
            for (let i = -1; i < origin.y; ++i) {
                const mesh = this.wallTemplates[randomInt(0, this.wallTemplates.length-1)].clone();
                mesh.position.copy(origin);
                mesh.position.y = i;
                this.scene.add(mesh);
                this.walls.push(mesh);
            }
        });

        this.pathLines.vertices.length = 0;
        game.testHeights.forEach(neighbours => {
            for (let i = 1; i < 5; ++i) {
                this.pathLines.vertices.push(neighbours[0], neighbours[i]);
            }
        });
    }

    private makeCharacterRenderer(): Mesh
    {
        const quad = new PlaneGeometry(1, 1);
        quad.rotateY(Math.PI /2);
        quad.translate(0, .5, 0);
        const renderer = new Mesh(quad, this.paletteMaterial);

        return renderer;
    }

    private billboardCharater(mesh: Mesh): void
    {
        const angle = Math.atan2(this.pivot.focus.z - this.camera.position.z,
                                 this.pivot.focus.x - this.camera.position.x);
        const rot = -angle-Math.PI;

        mesh.rotation.set(0, rot, 0);
    }
}
