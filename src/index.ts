import paletteVertURL from "./resources/palette.vert";
import paletteFragURL from "./resources/palette.frag";

import { SceneRenderer } from "./SceneRenderer";
import { Application } from "./application";
import Player from "./player";
import { InterfaceRenderer } from "./InterfaceRenderer";

import fontData from "./resources/ascii-small-font";
import localForage from "localforage";
import textureData from "./resources/texture-data";
import { decodeTexture, decodeFont } from "blitsy";

export async function start()
{
    localForage.config({name: "blocksy"});

    const paletteVertSource = await fetch(paletteVertURL).then(response => response.text());
    const paletteFragSource = await fetch(paletteFragURL).then(response => response.text());
    const font = decodeFont(fontData);

    const arrow = decodeTexture(textureData["dialogue-arrow"]).canvas;
    const square = decodeTexture(textureData["dialogue-square"]).canvas;

    const application = new Application(document, document.getElementById("renderer")!);
    const sceneRenderer = new SceneRenderer(application);
    const interfaceRenderer = new InterfaceRenderer(application, font, arrow, square);
    const player = new Player(application, sceneRenderer, interfaceRenderer);
    
    sceneRenderer.setPaletteMaterial(paletteVertSource, paletteFragSource);

    const input = document.getElementById("script-input") as HTMLTextAreaElement;
    const button = document.getElementById("script-preview") as HTMLButtonElement;

    button.addEventListener("click", () => {
        interfaceRenderer.cancelDialogue();
        interfaceRenderer.queueDialogueScript(input.value || "");
    });

    // play embeded game or open editor
    const embed = document.getElementById("flicksy-data");

    if (embed)
    {
        //editor.setProject(jsonToProject(embed.innerHTML));
        //editor.enterPlayback(false);
    }
    else
    {
        //const project = await findProject();
        //editor.setProject(newProject());
        //editor.enterEditor();
    }
}

start();
